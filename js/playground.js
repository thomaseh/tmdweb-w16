$(document).ready(function() {
	$(".active").removeClass("active");
	$("#playground_header").addClass("active");

	// Initializing Canvas
	var canvas = $("#canvas")[0];
	var canvas_context = canvas.getContext("2d");
	var width = $("#canvas").width();
	var height = $("#canvas").height();
	
	var cell_width = 15;
	var direction;
	var food;
	var score;
	var highscore = 0;
	var is_paused = false;
	var game_over = false;
	
	var snake_array; // an array of cells to make up the snake
	
	$("#start_button").click(function() {
		init();
		$(this).html("Reset");
	})

	function init() {
		direction = "right"; //default direction
		create_snake();
		create_food();

		score = 0;

		game_over = false;
		
		// timer that will trigger the paint function
		if (typeof game_loop != "undefined") clearInterval(game_loop);
		game_loop = setInterval(paint, 30);

	}
	
	function create_snake() {
	  	var length = 5;
	  	snake_array = [];
	  	for(var j = length - 1; j >= 0; j--)
	  		snake_array.push({x: j,
	  					  	  y: 0});
	}
	
	function create_food() {
	  	food = {
	    	x: Math.round(Math.random() * (width - cell_width) / cell_width), 
	    	y: Math.round(Math.random() * (height - cell_width) / cell_width), 
	  	};
	}
	
	//Lets paint the snake now
	function paint() {
		if (!is_paused) {
			
			if (game_over) {
				var high_score_text = "High Score: " + highscore;
				canvas_context.font = '30pt Calibri';
				canvas_context.textAlign = 'center';
				canvas_context.fillStyle = 'blue';
				canvas_context.fillText(high_score_text, width / 2, height / 2);

				return;
			}

			// paint canvas
			canvas_context.fillStyle = "white";
			canvas_context.fillRect(0, 0, width, height);
			canvas_context.strokeStyle = "black";
			canvas_context.strokeRect(0, 0, width, height);
			
			// position of the head of the snake
			var head_x = snake_array[0].x;
			var head_y = snake_array[0].y;
			// getting position in front of the head of the snake
			if (direction == "right") head_x++;
			else if (direction == "left") head_x--;
			else if (direction == "up") head_y--;
			else if (direction == "down") head_y++;
			
			// if snake hits itself, will die
			if(head_x == -1 || head_x == width / cell_width || head_y == -1 || head_y == height / cell_width || check_collision(head_x, head_y, snake_array)) {
				if (score > highscore) highscore = score;
				game_over = true;

				return;
			}
			
			// snake eating food
			if(head_x == food.x && head_y == food.y) {
				var tail = {x: head_x,
							y: head_y};
				score++;
				
				create_food();
			} else {
				var tail = snake_array.pop();
				tail.x = head_x;
				tail.y = head_y;
			}
			
			snake_array.unshift(tail); //puts back the tail as the first cell
			var h = snake_array[0];
			paint_cell(h.x, h.y, "blue");
			for(var i = 1; i < snake_array.length; i++) {
				var c = snake_array[i];
				paint_cell(c.x, c.y, "green");
			}
			
			paint_cell(food.x, food.y, "red");
			var score_text = "Score: " + score;
			canvas_context.font = '15pt Calibri';
			canvas_context.textAlign = 'center';
			canvas_context.fillStyle = 'blue';
			canvas_context.fillText(score_text, 40, height - 5);
		}
	}
	
	// function that paints cells
	function paint_cell(x, y, color) {
		canvas_context.fillStyle = color;
		canvas_context.fillRect(x * cell_width, y * cell_width, cell_width, cell_width);
		canvas_context.strokeStyle = "white";
		canvas_context.strokeRect(x * cell_width, y * cell_width, cell_width, cell_width);
	}
	
	// function that checks whether (x, y) exist in array
	function check_collision(x, y, array) {
		for(var i = 0; i < array.length; i++) {
			if(array[i].x == x && array[i].y == y) {
				return true;
			}
		}
		return false;
	}
	
	// keyboard controls
	$(document).keydown(function(e){
		var key = e.which;
		if (true) {
			if(key == "37" && direction != "right" && !game_over && !is_paused) {
				direction = "left";
			}
			else if (key == "38" && direction != "down" && !game_over && !is_paused) {
				direction = "up";
			}
			else if (key == "39" && direction != "left" && !game_over && !is_paused) {
				direction = "right";
			}
			else if (key == "40" && direction != "up" && !game_over && !is_paused) {
				direction = "down";
			}
			else if (key == "32" && !game_over) {
				is_paused = !is_paused;
			}
			else if (key == "82" && game_over) {
				init();
			}
		}
	})

	// $(document).keyup(function(e){
	// 	keyllowed[e.which] = true;
	// })

	window.addEventListener("keydown", function(e) {
    	// space and arrow keys
    	if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
    	    e.preventDefault();
    	}
	}, false);
})