# README #

Create a new directory on your local machine and fork this repository. You will be pushing your final project to this repository when you are completed with the project.

Create a three page website, using html, css, and javascript.

First page is personal biography and contains links to the two other pages.

Second page is a short write-up on what aspect(s) of the Michigan Daily website you think can be improved.

Third page is your playground. Create interactive dataviz, embed videos or images, mess with html, css, or javascript, or anything else. Just have fun and experiment.

Upload the pages and all associated files and create a pull request to the tmd directory.

Make sure to take advantage of all types of online resources if you have trouble using git or any of the other web development skills that are required to complete this application.

Resources:

http://www.w3schools.com/

https://www.javascript.com/

http://www.awwwards.com/

http://www.highcharts.com/

https://help.github.com/articles/fork-a-repo/

https://www.atlassian.com/git/tutorials/setting-up-a-repository